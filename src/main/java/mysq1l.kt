import java.sql.Connection

/**
 * Created by maratismagilov on 29.04.2018.
 */

import java.sql.DriverManager
import java.sql.SQLException
import java.sql.Statement

object JDBCExample {
    // JDBC driver name and database URL
    internal val JDBC_DRIVER = "com.kpfu.homework.testingdatabase.mysql.jdbc.Driver"
    internal val DB_URL = "jdbc:kpfu.homework.testingdatabase.mysql://localhost/"

    //  Database credentials
    internal val USER = "root"
    internal val PASS = "root"

    @JvmStatic
    fun main(args: Array<String>) {
        var conn: Connection? = null
        var stmt: Statement? = null
        try {
            //STEP 2: Register JDBC driver
            Class.forName("com.kpfu.homework.testingdatabase.mysql.jdbc.Driver")

            //STEP 3: Open a connection
            println("Connecting to database...")
            conn = DriverManager.getConnection(DB_URL, USER, PASS)

            //STEP 4: Execute a query
            println("Creating database...")
            stmt = conn!!.createStatement()

            val sql = "CREATE DATABASE mysqltest"
            stmt!!.executeUpdate(sql)
            println("Database created successfully...")
        } catch (se: SQLException) {
            //Handle errors for JDBC
            se.printStackTrace()
        } catch (e: Exception) {
            //Handle errors for Class.forName
            e.printStackTrace()
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close()
            } catch (se2: SQLException) {
            }
            // nothing we can do
            try {
                if (conn != null)
                    conn.close()
            } catch (se: SQLException) {
                se.printStackTrace()
            }
        }
        println("Goodbye!")
    }
}