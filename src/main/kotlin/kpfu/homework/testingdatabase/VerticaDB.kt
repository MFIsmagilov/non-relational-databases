package kpfu.homework.testingdatabase

import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import java.sql.Statement

/**
 * Created by maratismagilov on 17.05.2018.
 */
class VerticaDB(val user: String,
                val password: String): IDatabase {
    private val JDBC_DRIVER = "com.vertica.jdbc.Driver"
    private val DB_URL = "jdbc:vertica://localhost/"

    private var conn: Connection? = null
    private var stmt: Statement? = null
    private lateinit var databaseName: String
    override fun init(databaseName: String) {
        try {
            this.databaseName = databaseName
            Class.forName(JDBC_DRIVER)
            conn = DriverManager.getConnection(DB_URL, user, password)
            stmt = conn?.createStatement()
        } catch (se: SQLException) {
            se.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun createDatabase() {
        assert(!(conn == null || stmt == null))
        val sql = "CREATE DATABASE $databaseName"
        stmt?.executeUpdate(sql)
    }

    override fun removeDatabase() {
        assert(!(conn == null || stmt == null))
        val sql = "DROP DATABASE $databaseName"
        stmt?.executeUpdate(sql)
    }

    override fun createTables() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun removeTables() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun insert(account: Account) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun insert(company: Company) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAccountById(accountId: Int): Account? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findCompanyById(companyId: Int): Company? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun updateAccountByCompany(companyId: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findCompanyWithAccount(accountId: Int): Company? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun dispose() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}