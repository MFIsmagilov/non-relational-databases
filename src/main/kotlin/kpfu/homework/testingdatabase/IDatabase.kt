package kpfu.homework.testingdatabase

/**
 * Created by maratismagilov on 29.04.2018.
 */
interface IDatabase {

    fun init(databaseName: String)

    fun createDatabase()

    fun removeDatabase()

    //для mongo collection
    fun createTables()

    fun removeTables()

    fun insert(account: Account)

    fun insert(company: Company)

    fun findAccountById(accountId: Int): Account?

    fun findCompanyById(companyId: Int): Company?

    fun updateAccountByCompany(companyId: Int)

    fun findCompanyWithAccount(accountId: Int): Company?
//    fun find(accountId: Int): Account
//
//    fun find(companyId: Int): Account

    fun dispose()
}