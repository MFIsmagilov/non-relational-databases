package kpfu.homework.testingdatabase

import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import java.sql.Statement
import java.util.*

/**
 * Created by maratismagilov on 21.05.2018.
 */
class ColumnSql(val user: String, val pass: String) : IDatabase {

    private val JDBC_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver"
    private val DB_URL = "jdbc:sqlserver://MARAT-PC\\SQLEXPRESS;port=1433;integratedSecurity=true"

    private var conn: Connection? = null
    private var stmt: Statement? = null

    private lateinit var databaseName: String

    override fun init(databaseName: String) {
        try {
            this.databaseName = databaseName
            Class.forName(JDBC_DRIVER)
            conn = DriverManager.getConnection(DB_URL, "MaRaT-PC\\MaRaT", "")
            stmt = conn?.createStatement()
        } catch (se: SQLException) {
            se.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun createDatabase() {
        assert(!(conn == null || stmt == null))
        val sql = "CREATE DATABASE $databaseName"
        stmt?.executeUpdate(sql)
    }

    override fun removeDatabase() {
        assert(!(conn == null || stmt == null))
        val sql = "DROP DATABASE $databaseName"
        stmt?.executeUpdate(sql)
    }

    override fun createTables() {
        assert(!(conn == null || stmt == null))

        val sqlCompanyCreate = """
USE $databaseName;
CREATE TABLE company (
	id int NOT NULL,
	name varchar(50) NOT NULL,
	address varchar(255) NOT NULL,
	site varchar(255) DEFAULT NULL,
);
create nonclustered index cdxName on company(id, address, site);
create nonclustered index cdxAddress on company(id, name, site);
create nonclustered index cdxSite on company(id, name, address);
"""

        val sqlAccountCreate = """
USE $databaseName;
CREATE TABLE account(
        id int NOT NULL,
        idCompany int NOT NULL,
        username varchar(255) NOT NULL,
        password varchar(255) NOT NULL,
        email varchar(50) NOT NULL,
        tokenKey varchar(255) NOT NULL,
		PRIMARY KEY(id),
		CONSTRAINT FK_account_company_id FOREIGN KEY (idCompany)
        REFERENCES $databaseName.company (id)
)

go
create clustered index cdxId on account(idCompany, username, password, email, tokenKey);

go
create nonclustered index cdxCompany on account(id, username, password, email, tokenKey);

go
create nonclustered index cdxUsername on account(id, idCompany, password, email, tokenKey);

go
create nonclustered index cdxPassword on account(id, idCompany, username, email, tokenKey);

go
create nonclustered index cdxEmail on account(id, idCompany, username, password, tokenKey);

go
create nonclustered index cdxTokenKey on account(id, idCompany, username, password, email);
"""
        stmt?.execute(sqlCompanyCreate)
//        stmt?.execute("USE $databaseName; go create clustered index cdxId on company(name, address, site);")
//
//        go
//        create clustered index cdxId on company(name, address, site);
//
//        go
//        create nonclustered index cdxName on company(id, address, site);
//
//        go
//        create nonclustered index cdxAddress on company(id, name, site);
//
//        go
//        create nonclustered index cdxSite on company(id, name, address);
    }

    override fun removeTables() {
        val sqlAccountDrop = "DROP TABLE $databaseName.account;"
        val sqlCompanyDrop = "DROP TABLE $databaseName.company;"
        stmt?.executeUpdate(sqlAccountDrop)
        stmt?.executeUpdate(sqlCompanyDrop)
    }

    override fun insert(account: Account) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun insert(company: Company) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAccountById(accountId: Int): Account? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findCompanyById(companyId: Int): Company? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun updateAccountByCompany(companyId: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findCompanyWithAccount(accountId: Int): Company? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun dispose() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}