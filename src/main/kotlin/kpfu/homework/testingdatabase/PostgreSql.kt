package kpfu.homework.testingdatabase

import java.sql.Connection
import java.sql.Statement
import java.sql.DriverManager
import java.sql.SQLException


/**
 * Created by maratismagilov on 01.05.2018.
 */

class PostgreSql(val user: String,
                 val password: String) : IDatabase {
    override fun updateAccountByCompany(companyId: Int) {
        val sqlAccountUpdate = "  UPDATE account SET idCompany = $companyId;"
        stmt?.execute(sqlAccountUpdate)
    }

    private val DB_URL = "jdbc:postgresql://localhost:5432/test"

    private var conn: Connection? = null
    private var stmt: Statement? = null
    private lateinit var databaseName: String

    override fun init(databaseName: String) {
        try {
            this.databaseName = databaseName
            conn = DriverManager.getConnection(DB_URL, user, password)
            stmt = conn?.createStatement()
        } catch (se: SQLException) {
            se.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private val accountObjectName = "AccountObject"
    private val companyTableName = "CompanyTable"

    override fun createDatabase() {
        assert(!(conn == null || stmt == null))
        val sql = "CREATE DATABASE $databaseName"
        stmt?.executeUpdate(sql)
    }

    override fun removeDatabase() {
        assert(!(conn == null || stmt == null))
        val sql = "DROP DATABASE $databaseName"
        stmt?.executeUpdate(sql)
    }

    override fun createTables() {
        assert(!(conn == null || stmt == null))
        val sqlAccountCreate = """
CREATE TYPE $accountObjectName as (id int, username text, password text, email text, tokenKey text);
"""
        val sqlCompanyCreate = """
CREATE TABLE $companyTableName (
    id      int PRIMARY KEY,
    name            text NOT NULL,
    address         text NOT NULL,
    site            text DEFAULT NULL,
    accounts        AccountObject[]
);
"""
        stmt?.executeUpdate(sqlAccountCreate)
        stmt?.executeUpdate(sqlCompanyCreate)
    }

    override fun removeTables() {
        val sqlAccountDrop = "DROP TYPE $accountObjectName"
        val sqlCompanyDrop = "DROP TABLE $companyTableName"
        stmt?.executeUpdate(sqlCompanyDrop)
        stmt?.executeUpdate(sqlAccountDrop)
    }

    override fun insert(account: Account) {
        val sqlAccount = """
UPDATE $companyTableName
  SET accounts = array_append(accounts,
  ROW(
  ${account.id},
  ${account.username},
  ${account.password},
  ${account.email},
  ${account.tokenKey}) :: $accountObjectName)
WHERE id=${account.idCompany.id}
"""
        stmt?.executeUpdate(sqlAccount)
    }

    override fun insert(company: Company) {
        val sqlCompany = """
INSERT INTO CompanyTable VALUES (${company.id}, 'name', 'address', 'site',
    ARRAY[]::AccountObject[])"""
        stmt?.executeUpdate(sqlCompany)
    }

    override fun findAccountById(accountId: Int): Account? {
        return null
    }

    override fun findCompanyById(companyId: Int): Company? {
        return null
    }

    override fun findCompanyWithAccount(accountId: Int): Company? {
        val sql = """
select * from $companyTableName as ct1
    where 1 = any(select (unnest(accounts)).id from $companyTableName  as ct2
    where ct1.id = ct2.id)"""
        stmt?.execute(sql)
        return null
    }

    override fun dispose() {
        stmt?.close()
        conn?.close()
    }

}