package kpfu.homework.testingdatabase

import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import java.sql.Statement

/**
 * Created by maratismagilov on 29.04.2018.
 */

class MySql(val user: String,
            val password: String) : IDatabase {

    override fun updateAccountByCompany(companyId: Int) {
        val sqlAccountUpdate = "UPDATE $databaseName.account SET idCompany = 999"
        stmt?.execute(sqlAccountUpdate)
    }

    private val JDBC_DRIVER = "com.mysql.jdbc.Driver"
    private val DB_URL = "jdbc:mysql://localhost/"

    private var conn: Connection? = null
    private var stmt: Statement? = null
    private lateinit var databaseName: String
    override fun init(databaseName: String) {
        try {
            this.databaseName = databaseName
            Class.forName(JDBC_DRIVER)
            conn = DriverManager.getConnection(DB_URL, user, password)
            stmt = conn?.createStatement()
        } catch (se: SQLException) {
            se.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun createDatabase() {
        assert(!(conn == null || stmt == null))
        val sql = "CREATE DATABASE $databaseName"
        stmt?.executeUpdate(sql)
    }

    override fun removeDatabase() {
        assert(!(conn == null || stmt == null))
        val sql = "DROP DATABASE $databaseName"
        stmt?.executeUpdate(sql)
    }

    override fun createTables() {
        assert(!(conn == null || stmt == null))

        val sqlCompanyCreate = """
CREATE TABLE $databaseName.company (
        id int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        name varchar(50) NOT NULL,
        address varchar(255) NOT NULL,
        site varchar(255) DEFAULT NULL,
        PRIMARY KEY (id))
        ENGINE = INNODB
        AUTO_INCREMENT = 1004820
        AVG_ROW_LENGTH = 16384
        CHARACTER SET utf8
        COLLATE utf8_general_ci;
"""
        val sqlAccountCreate = """
CREATE TABLE $databaseName.account (
        id int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        idCompany int(10) UNSIGNED NOT NULL,
        username varchar(255) NOT NULL,
        password varchar(255) NOT NULL,
        email varchar(50) NOT NULL,
        tokenKey varchar(255) NOT NULL,
        PRIMARY KEY (id),
        CONSTRAINT FK_account_company_id FOREIGN KEY (idCompany)
        REFERENCES $databaseName.company (id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
ENGINE = INNODB
AUTO_INCREMENT = 1004819
AVG_ROW_LENGTH = 69
CHARACTER SET utf8
COLLATE utf8_general_ci;
"""
        stmt?.executeUpdate(sqlCompanyCreate)
        stmt?.executeUpdate(sqlAccountCreate)
    }

    override fun removeTables() {
        val sqlAccountDrop = "DROP TABLE $databaseName.account;"
        val sqlCompanyDrop = "DROP TABLE $databaseName.company;"
        stmt?.executeUpdate(sqlAccountDrop)
        stmt?.executeUpdate(sqlCompanyDrop)
    }


    override fun insert(company: Company) {
        val sqlInsert = "INSERT INTO $databaseName.company VALUE (${company.id}, '${company.name}', '${company.address}', '${company.site}');"
        stmt?.execute(sqlInsert)
    }

    override fun insert(account: Account) {
        val sqlAccountInsert = "INSERT INTO $databaseName.account VALUE (${account.id}, ${account.idCompany.id}, '${account.username}', '${account.password}', '${account.email}', '${account.tokenKey}')"
        stmt?.execute(sqlAccountInsert)
    }

    override fun findAccountById(accountId: Int): Account? {
        val sqlAccountFind = "SELECT * FROM $databaseName.account WHERE id=$accountId"
        stmt?.execute(sqlAccountFind)
        return null //я просто тестирую скорость запроса ля-ля-ля
    }

    override fun findCompanyById(companyId: Int): Company? {
        val sqlCompanyFind = "SELECT * FROM $databaseName.company WHERE id=$companyId"
        stmt?.execute(sqlCompanyFind)
        return null //я просто тестирую скорость запроса ля-ля-ля
    }

    override fun findCompanyWithAccount(accountId: Int): Company? {
        val sql = """SELECT idCompany FROM $databaseName.account WHERE
            """
        stmt?.execute(sql)
        return null
    }

    override fun dispose() {
        stmt?.close()
        conn?.close()
    }
}