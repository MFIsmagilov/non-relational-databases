package kpfu.homework.testingdatabase

/**
 * Created by maratismagilov on 29.04.2018.
 */


val DATABASE_NAME = "testing_database"
val DATABASE_URL = "jdbc:kpfu.homework.testingdatabase.mysql://localhost/"
val USER = "root"
val PASS = "root"

data class Company(
        val id: Int,
        val name: String,
        val address: String,
        val site: String
)

data class Account(
        val id: Int,
        val idCompany: Company,
        val username: String,
        val password: String,
        val email: String,
        val tokenKey: String
)

//аккаунты списком у Company //для Postgre
