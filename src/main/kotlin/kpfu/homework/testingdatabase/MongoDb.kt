package kpfu.homework.testingdatabase

import com.google.gson.Gson
import com.mongodb.*
import com.sun.org.glassfish.external.amx.AMXUtil.prop
import com.sun.xml.internal.fastinfoset.alphabet.BuiltInRestrictedAlphabets.table
import org.postgresql.gss.MakeGSS.authenticate
import com.mongodb.client.MongoDatabase
import java.net.UnknownHostException
import java.util.*


/**
 * Created by maratismagilov on 02.05.2018.
 */
class MongoDb(val user: String, val pass: String) : IDatabase {

    lateinit var mongoClient: MongoClient
    lateinit var db: MongoDatabase
    lateinit var table: DBCollection
    lateinit var databaseName: String

    override fun init(databaseName: String) {
        try {
            this.databaseName = databaseName

            mongoClient = MongoClient(MongoClientURI("mongodb://$user:$pass@127.0.0.1:27017"))
            db = mongoClient.getDatabase(databaseName)
        } catch (e: UnknownHostException) {
            System.err.println("Don't connect!")
        }
    }

    override fun createDatabase() {
//        val obj = BasicDBObject()
//        obj["accounts", Gson().toJson(Account::class.java)]
//        db.createCollection("account", )
    }

    override fun removeDatabase() {
    }

    override fun createTables() {
    }

    override fun removeTables() {
    }

    override fun insert(account: Account) {
    }

    override fun insert(company: Company) {
    }

    override fun findCompanyWithAccount(accountId: Int): Company? = null

    override fun findAccountById(accountId: Int): Account? = null

    override fun findCompanyById(companyId: Int): Company? = null

    override fun updateAccountByCompany(companyId: Int) {
    }

    override fun dispose() {
    }
}