package kpfu.homework.testingdatabase.mysql

import kpfu.homework.testingdatabase.Account
import kpfu.homework.testingdatabase.Company
import kpfu.homework.testingdatabase.GeneratorData
import org.junit.Before
import org.junit.BeforeClass
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.RepeatedTest
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.junit.jupiter.params.provider.ValueSource
import java.util.stream.Stream


/**
 * Created by maratismagilov on 29.04.2018.
 */
//@RunWith(value = Parameterized::class)
class InsertIntoTablesMySql : MySqlTest() {

    companion object {
        @JvmStatic
        fun getAccounts(): Stream<Account>? {
            return GeneratorData.getAccounts()
        }
    }

    class PrepareDataBase : MySqlTest() {
        @RepeatedTest(1)
        fun prepareDatabase() {
            init()
            database.removeTables()
            database.createTables()
        }
    }


    @ParameterizedTest
    @MethodSource("getAccounts")
    fun test(account: Account) {
        database.insert(account.idCompany)
        database.insert(account)
    }
}