package kpfu.homework.testingdatabase.mysql

import kpfu.homework.testingdatabase.Account
import kpfu.homework.testingdatabase.GeneratorData
import org.junit.jupiter.api.RepeatedTest
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

/**
 * Created by maratismagilov on 29.04.2018.
 */
class FindInAccountMySql : MySqlTest() {

    companion object {
        @JvmStatic
        fun getAccounts(): Stream<Account>? {
            return GeneratorData.getAccounts()
        }
    }

    @ParameterizedTest
    @MethodSource("getAccounts")
    fun test(account: Account) {
        database.findCompanyById(account.idCompany.id)
    }
}