package kpfu.homework.testingdatabase.mysql

import kpfu.homework.testingdatabase.common.DatabaseTest
import kpfu.homework.testingdatabase.MySql
import kpfu.homework.testingdatabase.PASS
import kpfu.homework.testingdatabase.USER

/**
 * Created by maratismagilov on 29.04.2018.
 */
abstract class MySqlTest : DatabaseTest() {
    override fun init() {
        database = MySql(USER, PASS)
        databaseName = "testing_mysql"
        //todo: почему бы не передать в конструктор? что за дичь?
        database.init(databaseName)
    }
}