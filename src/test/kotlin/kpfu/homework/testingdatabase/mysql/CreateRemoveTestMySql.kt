package kpfu.homework.testingdatabase.mysql

import org.junit.FixMethodOrder
import org.junit.jupiter.api.RepeatedTest
import org.junit.runners.MethodSorters

/**
 * Created by maratismagilov on 29.04.2018.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class CreateRemoveTestMySql : MySqlTest() {

    @RepeatedTest(100)
    fun testingCreateRemove() {
        createRemove()
    }

    @RepeatedTest(1)
    fun createT() {
        createDatabaseAndTables()
    }

    @RepeatedTest(1)
    fun removeT() {
        removeTablesAndDatabse()
    }
}