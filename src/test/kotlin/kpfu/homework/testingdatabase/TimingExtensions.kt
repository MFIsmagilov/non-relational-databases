package kpfu.homework.testingdatabase

/**
 * Created by maratismagilov on 29.04.2018.
 */
import org.junit.jupiter.api.extension.*
import java.util.logging.Logger

import org.junit.jupiter.api.extension.ExtensionContext.Namespace
import org.junit.jupiter.api.extension.ExtensionContext.Store
import org.junit.runner.Result
import org.junit.runner.notification.RunListener


class Info(val count: Int, val time: Long)

class TimingExtension : AfterAllCallback, BeforeTestExecutionCallback, AfterTestExecutionCallback, RunListener() {

    private val map: MutableMap<String, Info> = mutableMapOf()

    override fun afterAll(context: ExtensionContext?) {


        map.forEach { key, value ->
            println("$key : ${value.time} ms")
            println("$key : ${value.time / value.count} ms")
        }
    }

    override fun testRunFinished(result: Result?) {
    //    logger.info("!DSACASLA")
    }

    @Throws(Exception::class)
    override fun beforeTestExecution(context: ExtensionContext) {
        val time = System.currentTimeMillis()
        getStore(context).put(START_TIME, time)
    }

    @Throws(Exception::class)
    override fun afterTestExecution(context: ExtensionContext) {
        val startTime = getStore(context).remove(START_TIME, Long::class.javaPrimitiveType)
        val duration = System.currentTimeMillis() - startTime
//        println(duration)
        val testMethod = context.requiredTestMethod

        val methodName = testMethod.name
        if (map[methodName] == null) {
            map[methodName] = Info(1, duration)
        } else {
            val old = map[methodName]
            old?.let {
                map[methodName] = Info(it.count + 1, it.time + duration)
            }
        }
      //  logger.info { String.format("%s : %s", testMethod.name, duration) }
    }


    private fun getStore(context: ExtensionContext): Store {
        return context.getStore(Namespace.create(javaClass, context.requiredTestMethod))
    }

    companion object {

        private val logger = Logger.getLogger(TimingExtension::class.java.name)

        private val START_TIME = "start time"
    }

}