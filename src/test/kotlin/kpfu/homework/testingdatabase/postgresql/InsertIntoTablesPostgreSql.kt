package kpfu.homework.testingdatabase.postgresql

import javafx.geometry.Pos
import kpfu.homework.testingdatabase.Account
import kpfu.homework.testingdatabase.GeneratorData
import kpfu.homework.testingdatabase.mysql.MySqlTest
import org.junit.jupiter.api.RepeatedTest
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

/**
 * Created by maratismagilov on 01.05.2018.
 */
class InsertIntoTablesPostgreSql : PostgreSqlTest() {

    companion object {
        @JvmStatic
        fun getAccounts(): Stream<Account>? {
            return GeneratorData.getAccounts()
        }
    }

    class PrepareDataBase : PostgreSqlTest() {
        @RepeatedTest(1)
        fun prepareDatabase() {
            init()
            database.removeTables()
            database.createTables()
        }
    }


    @ParameterizedTest
    @MethodSource("getAccounts")
    fun test(account: Account) {
        database.insert(account.idCompany)
        database.insert(account)
    }
}