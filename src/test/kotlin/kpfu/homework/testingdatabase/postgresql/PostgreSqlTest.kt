package kpfu.homework.testingdatabase.postgresql

import kpfu.homework.testingdatabase.*
import kpfu.homework.testingdatabase.common.DatabaseTest

/**
 * Created by maratismagilov on 01.05.2018.
 */
open class PostgreSqlTest: DatabaseTest() {
    override fun init() {
        database = PostgreSql("postgres", PASS)
        databaseName = "testing_postgresql"
        //todo: почему бы не передать в конструктор? что за дичь?
        database.init(databaseName)
    }
}