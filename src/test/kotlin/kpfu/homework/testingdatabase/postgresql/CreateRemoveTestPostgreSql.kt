package kpfu.homework.testingdatabase.postgresql

import org.junit.FixMethodOrder
import org.junit.jupiter.api.RepeatedTest
import org.junit.runners.MethodSorters

/**
 * Created by maratismagilov on 01.05.2018.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class CreateRemoveTestPostgreSql : PostgreSqlTest() {

    @RepeatedTest(100)
    fun testingCreateRemove() {
        createRemove()
    }

    @RepeatedTest(1)
    fun createT() {
        createDatabaseAndTables()
    }

    @RepeatedTest(1)
    fun removeT() {
        removeTablesAndDatabse()
    }
}