package kpfu.homework.testingdatabase.postgresql

import kpfu.homework.testingdatabase.Account
import kpfu.homework.testingdatabase.GeneratorData
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

/**
 * Created by maratismagilov on 16.05.2018.
 */
class FindCompanyWithAccount : PostgreSqlTest() {

    companion object {
        @JvmStatic
        fun getAccounts(): Stream<Account>? {
            return GeneratorData.getAccounts()
        }
    }

    @ParameterizedTest
    @MethodSource("getAccounts")
    fun test(account: Account) {
        database.findCompanyWithAccount(account.idCompany.id)
    }
}