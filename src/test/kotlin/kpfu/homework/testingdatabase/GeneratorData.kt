package kpfu.homework.testingdatabase

import kpfu.homework.testingdatabase.Account
import kpfu.homework.testingdatabase.Company
import java.util.stream.Stream

/**
 * Created by maratismagilov on 29.04.2018.
 */
class GeneratorData {

    companion object {

        private val countUser = 1_100

        @JvmStatic
        fun getAccounts(): Stream<Account>? {
            val list = GeneratorData.generate(countUser)
            val streams = Stream.builder<Account>()
            list.forEach {
                streams.add(it)
            }
            return streams.build()
        }

        fun generate(count: Int): List<Account> {
            val list = mutableListOf<Account>()
            for (i in 1..count) {
                val istr = i.toString()
                val company = Company(i, istr, istr, istr)
                val account = Account(i, company, istr, istr, istr, istr)
                list.add(account)
            }
            return list
        }
    }
}