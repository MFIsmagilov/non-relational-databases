package kpfu.homework.testingdatabase.common

import kpfu.homework.testingdatabase.Account
import kpfu.homework.testingdatabase.GeneratorData
import kpfu.homework.testingdatabase.IDatabase
import kpfu.homework.testingdatabase.TimingExtension
import org.junit.After
import org.junit.Before
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.extension.ExtendWith
import java.util.stream.Stream

/**
 * Created by maratismagilov on 29.04.2018.
 */
@ExtendWith(TimingExtension::class)
abstract class DatabaseTest {

    lateinit var database: IDatabase
    lateinit var databaseName: String
    lateinit var tableName: String

    abstract fun init()

    @Before
    @BeforeEach
    open fun before() {
        init()
    }

    @After
    @AfterEach
    fun after() {
        database.dispose()
    }

    //region create/remove
    fun createRemove() {
        createDatabaseAndTables()
        removeTablesAndDatabse()
    }

    fun createDatabaseAndTables() {
        database.createDatabase()
        database.createTables()
    }

    fun removeTablesAndDatabse() {
        database.removeTables()
        database.removeDatabase()
    }
    //endregion
}