package kpfu.homework.testingdatabase

/**
 * Created by maratismagilov on 29.04.2018.
 */
import org.junit.internal.AssumptionViolatedException
import org.junit.runner.Description
import org.junit.runner.Result
import org.junit.runner.notification.Failure
import org.junit.runner.notification.RunListener
import org.junit.runner.notification.RunNotifier
import org.junit.runners.BlockJUnit4ClassRunner
import org.junit.runners.model.InitializationError;

class SeleniumRunListener : RunListener() {
    @Throws(Exception::class)
    override fun testRunStarted(description: Description?) {
    }


    @Throws(Exception::class)
    override fun testRunFinished(result: Result?) {
        println("Result of the test run:")
        println("Run time: " + result!!.runTime + " ms")
        println("Run count: " + result.runCount)
        println("Failure count: " + result.failureCount)
        println("Ignored count: " + result.ignoreCount)
    }


    private var timeTestStart = 0L;
    @Throws(Exception::class)
    override fun testStarted(description: Description?) {
        println("Test starts: " + description!!)
        timeTestStart = System.currentTimeMillis()
    }

    @Throws(Exception::class)
    override fun testFinished(description: Description?) {
        val timeTestStop = System.currentTimeMillis();
        println("Test finished (${timeTestStop - timeTestStart} ms): " + description!!)
        println("--------------------------------------")
    }

    /**
     * Вызывается когда тест завершается неудачей.
     * @param failure описывает тест, который завершился ошибкой
     * и полученное исключение.
     */
    @Throws(Exception::class)
    override fun testFailure(failure: Failure?) {
        println("Test failed with: " + failure!!.exception)
    }

    /**
     * Вызывается когда не выполняется условие в классе Assume
     *
     * @param failure описывает тест, который не был выполнен
     * и исключение [AssumptionViolatedException]
     */
    override fun testAssumptionFailure(failure: Failure?) {
        println("Test assumes: " + failure!!.exception)
    }

    /**
     * Вызывается когда тест не будет запущен,
     * в основном потому что стоит аннотация @Ignore
     *
     * @param description описание теста который не будет запущен
     */
    @Throws(Exception::class)
    override fun testIgnored(description: Description?) {
        println("Test ignored: " + description!!)
        println("--------------------------------------")
    }
}

class SeleniumRunner @Throws(InitializationError::class)
constructor(klass: Class<*>) : BlockJUnit4ClassRunner(klass) {

    private val seleniumRunListener: SeleniumRunListener

    init {
        seleniumRunListener = SeleniumRunListener()
    }

    override fun run(notifier: RunNotifier) {
        notifier.addListener(seleniumRunListener)
        super.run(notifier)
    }
}